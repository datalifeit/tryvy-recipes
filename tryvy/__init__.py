# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import sh
from pythonforandroid.logger import shprint, info
from pythonforandroid.util import current_directory
from pythonforandroid.recipe import PythonRecipe


class Tryvy(PythonRecipe):

    version = '6.0'
    url = ('https://gitlab.com/datalifeit/tryvy-tryvy/-/'
        'archive/{version}/tryvy-{version}.zip')
    depends = [
        'python3',
        'setuptools',
        'openssl',
        'tryton_rpc',
        'android',
        'kivy',
        'kivymd',
        'python-dateutil'
    ]
    site_packages_name = 'tryvy'
    call_hostpython_via_targetpython = False

    def build_arch(self, arch):
        self.compile_catalogs(arch)
        super(Tryvy, self).build_arch(arch)

    def compile_catalogs(self, arch):
        '''Automate PO catalogs compilation'''
        env = self.get_recipe_env(arch)

        info('Compiling catalogs {} into site-packages'.format(
            self.name))

        with current_directory(self.get_build_dir(arch.arch)):
            hostpython = sh.Command(self.hostpython_location)
            shprint(hostpython, 'setup.py', 'compile_catalog',
                _env=env)


recipe = Tryvy()
