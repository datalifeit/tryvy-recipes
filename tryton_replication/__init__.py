# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from pythonforandroid.recipe import PythonRecipe


class TrytonReplication(PythonRecipe):

    version = '6.0'
    url = ('https://gitlab.com/datalifeit/tryton_replication/-/'
        'archive/{version}/tryton_replication-{version}.zip' % {
            'version': version})
    depends = ['python3', 'setuptools']
    site_packages_name = 'tryton_replication'
    call_hostpython_via_targetpython = False
    patches = ['logger.patch']


recipe = TrytonReplication()
